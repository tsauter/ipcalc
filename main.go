package main

import (
	"fmt"
	"html/template"
	"net"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

type Page struct {
	Error         string
	IP            string
	SingleOctets  []string
	SingleBits    []string
	SingleBitsPos []string
	HexGroup      []string
	BloatedIP     string
}

func handlerIP4Visualizer(w http.ResponseWriter, r *http.Request) {
	// load the template
	t, err := template.ParseFiles("ip4-visualizer.html")
	if err != nil {
		fmt.Printf("E: %s\n", err)
		http.Error(w, "HTML template error", http.StatusInternalServerError)
		return
	}

	// this struct holds all data for the template rendering
	p := Page{}

	// try to parse the specified IP address
	rawip := r.FormValue("ip")
	if rawip == "" {
		rawip = "192.168.0.1"
	}
	ip := net.ParseIP(rawip)
	// invalid IP
	if ip == nil {
		p.Error = fmt.Sprintf("failed to parse IP: %s\n", rawip)
		fmt.Printf("E: %s\n", p.Error)
		// Render the template
		w.WriteHeader(http.StatusOK)
		err := t.Execute(w, p)
		if err != nil {
			fmt.Printf("E: %s\n", err)
			http.Error(w, "HTML rendering failed", http.StatusInternalServerError)
		}
		return
	}

	p.IP = ip.String()

	// walk over each 16-byte slice element and store the HEX and BIT representation
	// in two new string slices. The string slice contains the expected format as
	// string (HEX: 2001, BIT: 00100000).
	// The content of these slices can be used, without further conversation, by the web page.
	//
	// Examples:
	// 2001:0db8:  :2
	//   [0]:
	//         0x20
	//         00100000
	//   [1]:
	//         0x01
	//         00000001
	//   [3]:
	//         0x0d
	//         00001101
	//   [4]:
	//         0xb8
	//         10111000
	//   ...
	//   [16]
	//         0x02
	//         00000010

	for y, b := range ip[12:] {
		octetstring := fmt.Sprintf("%03d", b)
		p.SingleOctets = append(p.SingleOctets, octetstring)

		// SingleBits contains each digit as a seperate entry (not only the octets).
		// SingleBitsPos is used the summarize the number of bits (IPv6 == 128)
		bitsstring := fmt.Sprintf("%08b", b)
		p.SingleBits = append(p.SingleBits, bitsstring)
		p.SingleBitsPos = append(p.SingleBitsPos, fmt.Sprintf("%d", (y+1)))
	}

	p.BloatedIP = strings.Join(p.SingleOctets, ".")

	// Render the template
	w.WriteHeader(http.StatusOK)
	err = t.Execute(w, p)
	if err != nil {
		fmt.Printf("E: %s\n", err)
		http.Error(w, "HTML rendering failed", http.StatusInternalServerError)
		return
	}

	return
}
func handlerIP6Visualizer(w http.ResponseWriter, r *http.Request) {
	// load the template
	t, err := template.ParseFiles("ip6-visualizer.html")
	if err != nil {
		fmt.Printf("E: %s\n", err)
		http.Error(w, "HTML template error", http.StatusInternalServerError)
		return
	}

	// this struct holds all data for the template rendering
	p := Page{}

	// try to parse the specified IP address
	rawip := r.FormValue("ip")
	if rawip == "" {
		rawip = "2001:db8::1"
	}
	ip := net.ParseIP(rawip)
	// invalid IP
	if ip == nil {
		p.Error = fmt.Sprintf("failed to parse IP: %s\n", rawip)
		fmt.Printf("E: %s\n", p.Error)
		// Render the template
		w.WriteHeader(http.StatusOK)
		err := t.Execute(w, p)
		if err != nil {
			fmt.Printf("E: %s\n", err)
			http.Error(w, "HTML rendering failed", http.StatusInternalServerError)
		}
		return
	}

	p.IP = ip.String()

	// walk over each 16-byte slice element and store the HEX and BIT representation
	// in two new string slices. The string slice contains the expected format as
	// string (HEX: 2001, BIT: 00100000).
	// The content of these slices can be used, without further conversation, by the web page.
	//
	// Examples:
	// 2001:0db8:  :2
	//   [0]:
	//         0x20
	//         00100000
	//   [1]:
	//         0x01
	//         00000001
	//   [3]:
	//         0x0d
	//         00001101
	//   [4]:
	//         0xb8
	//         10111000
	//   ...
	//   [16]
	//         0x02
	//         00000010

	y := 1 // used to store the total number of bits (IPv6 == 128 bits)
	for _, b := range ip {
		octetstring := fmt.Sprintf("%02x", b)
		for i := 0; i < len(octetstring); i++ {
			p.SingleOctets = append(p.SingleOctets, string(octetstring[i]))
		}

		// SingleBits contains each digit as a seperate entry (not only the octets).
		// SingleBitsPos is used the summarize the number of bits (IPv6 == 128)
		bitsstring := fmt.Sprintf("%08b", b)
		for i := 0; i < len(bitsstring); i++ {
			p.SingleBits = append(p.SingleBits, string(bitsstring[i]))
			p.SingleBitsPos = append(p.SingleBitsPos, fmt.Sprintf("%d", y))
			y++
		}

	}

	// HexGroup contains each group of the eight hex digits (with all zeros)
	// Example
	//   [0]: 2001
	//   [1]: 0db8
	p.HexGroup = append(p.HexGroup, getOctet(&p.SingleOctets, 0))
	p.HexGroup = append(p.HexGroup, getOctet(&p.SingleOctets, 4))
	p.HexGroup = append(p.HexGroup, getOctet(&p.SingleOctets, 8))
	p.HexGroup = append(p.HexGroup, getOctet(&p.SingleOctets, 12))
	p.HexGroup = append(p.HexGroup, getOctet(&p.SingleOctets, 16))
	p.HexGroup = append(p.HexGroup, getOctet(&p.SingleOctets, 20))
	p.HexGroup = append(p.HexGroup, getOctet(&p.SingleOctets, 24))
	p.HexGroup = append(p.HexGroup, getOctet(&p.SingleOctets, 28))

	p.BloatedIP = strings.Join(p.HexGroup, ":")

	// Render the template
	w.WriteHeader(http.StatusOK)
	err = t.Execute(w, p)
	if err != nil {
		fmt.Printf("E: %s\n", err)
		http.Error(w, "HTML rendering failed", http.StatusInternalServerError)
		return
	}

	return
}

// getOctet extract the next four bits from from the current position
func getOctet(singleOctets *[]string, pos int) string {
	var tmp string
	for i := pos; i < pos+4; i++ {
		tmp += (*singleOctets)[i]
	}
	return tmp
}

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/ip4/visualizer", handlerIP4Visualizer).Methods("GET", "POST")
	r.HandleFunc("/ip6/visualizer", handlerIP6Visualizer).Methods("GET", "POST")

	err := http.ListenAndServe("[::1]:8080", r)
	if err != nil {
		fmt.Printf("E: failed to start server: %s\n", err)
		return
	}
}
